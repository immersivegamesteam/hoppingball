﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesBounce : Obstacles
{
    [SerializeField]
    private float forwardForce;
    [SerializeField]
    private float jumpForce;
    protected override void OnEnable()
    {
        base.OnEnable();
        forwardforce = (forwardForce != 0) ? forwardForce : forwardforce;
        jumpforce = (jumpForce != 0) ? jumpForce : jumpforce;
    }
}
