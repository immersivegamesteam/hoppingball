﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Areas : MonoBehaviour
{
    protected Rigidbody player;
    protected GameManager gameManager;
    protected virtual void OnEnable()
    {
        gameManager = GameManager.Instance;
    }
}
