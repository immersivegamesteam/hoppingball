﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpeed : Obstacles
{
    [SerializeField]
    private float acceleration = 2;
    [SerializeField]
    private float speedyTime = 5;
    protected override void OnCollisionEnter(Collision collision)
    {
        player = collision.rigidbody;
        if (player && collision.gameObject.GetComponent<Players>())
        {
            collision.gameObject.GetComponent<Players>().SpeedyUp(acceleration, speedyTime);
        }
        base.OnCollisionEnter(collision);
    }
}
