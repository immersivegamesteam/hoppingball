﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersAuto : Players
{
    [SerializeField]
    private float agility = 400;
    private void FixedUpdate()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            rigplayer.AddForce(agility * Time.deltaTime, 0, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            rigplayer.AddForce(-agility * Time.deltaTime, 0, 0);
        }
        if (rigplayer.transform.position.y < gameManager.GetFallOut*2 && playerOut == false)
        {
            playerOut = true;
            this.gameObject.SetActive(false);
        }
    }
}
