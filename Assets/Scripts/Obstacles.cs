﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Obstacles : MonoBehaviour
{
    protected float forwardforce;
    protected float jumpforce;
    protected Rigidbody player;
    private GameManager gameManager;
    protected virtual void OnEnable()
    {
        gameManager = GameManager.Instance;
        forwardforce = gameManager.GetSpeedy;
        jumpforce = gameManager.GetJump;
    }
    protected virtual void OnCollisionEnter(Collision collision)
    {
        player = collision.rigidbody;
        if (player && collision.gameObject.GetComponent<Players>() && gameManager.ShouldBePlayingGame())
        {
            float ac = collision.gameObject.GetComponent<Players>().GetAcceleration;
            forwardforce = (forwardforce == 0) ? player.velocity.z : forwardforce;
            jumpforce = (jumpforce == 0) ? player.velocity.y : jumpforce;
            player.velocity = new Vector3(player.velocity.x, jumpforce, forwardforce * ac);
        }
    }
}
