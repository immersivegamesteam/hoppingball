﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private float defaultForward = 5;
    [SerializeField]
    private float defaultJump = 4;
    [SerializeField]
    private float defaultAccelerate = 1;
    [SerializeField]
    private bool gameOver, gamePause, gameFinish;
    [SerializeField]
    public List<Players> listPlayers;
    [SerializeField]
    private float fallOut = -3;

    public float GetSpeedy { get { return defaultForward; } }
    public float GetJump { get { return defaultJump; } }
    public float GetAccelerate { get { return defaultAccelerate; } }
    public float GetFallOut { get { return fallOut; } }

    public bool GameOver { get { return gameOver; } set { gameOver = value; } }
    public bool GamePause { get { return gamePause; } set { gamePause = value; } }
    public bool GameFinish { get { return gameFinish; } }

    public List<Players> GetListPlay { get { return listPlayers; } }
    public void AddPlayerList(Players player)
    {
        if(!listPlayers.Contains(player))
            listPlayers.Add(player);
    }

    public bool FinishGame
    {
        get
        {
            if (ShouldBePlayingGame())
            {
                gameFinish = true;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private IEnumerator Start()
    {
        StartCoroutine(GameLoop());
        yield return null;
    }
    private IEnumerator GameLoop()
    {
        SetConfigGame();
        // courotina de inicio das missões
        yield return StartCoroutine(MissionStarting());

        // courotina da missão em si
        yield return StartCoroutine(MissionPlaying());

        yield return StartCoroutine(GameOverState());

        // courotina do final da missão
        yield return StartCoroutine(MissionEnding());

        yield return StartCoroutine(BeatGameState());
    }

    private IEnumerator BeatGameState()
    {
        Debug.Log("Terminou o jogo");
        yield return null;
    }

    private IEnumerator MissionEnding()
    {
        Debug.Log("Fim de nivel");
        yield return null;
    }

    private IEnumerator GameOverState()
    {
        while (GameOver)
        {
            Debug.Log("Game over");
            yield return null;
        }
    }

    private IEnumerator MissionPlaying()
    {
        while (ShouldBePlayingGame())
        {
            Debug.Log("Playing ...");
            yield return null;
        }

    }

    private IEnumerator MissionStarting()
    {
        while (gamePause && gameOver == false)
        {
            Debug.Log("Starting");
            yield return null;
            if (Input.anyKey)
            {
                gamePause = false;
            }
        }   
    }

    private void SetConfigGame()
    {
        gameOver = false;
        gamePause = true;
        gameFinish = false;
        foreach (Players item in listPlayers)
        { 
            item.ResetPlayer();
            //item.gameObject.SetActive(true);
            //item.playerOut = false;
        }
    }

    public bool InGame()
    {
        return (!gameOver && !gamePause) ? true : false;
    }
    public bool ShouldBePlayingGame()
    {
        return ( gameOver == false && gameFinish==false && gamePause == false) ? true : false;
    }

    public void ResetScene()
    {
        StopAllCoroutines();
        StartCoroutine(GameLoop());
        gamePause = true;
    }
}
