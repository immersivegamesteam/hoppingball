﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersController : Players
{
    [SerializeField]
    private float agility = 400;
    private Transform camTransform;
    private Vector3 camPosition;
    private Quaternion camRotation;
    private float timeToRespawn = 5;

    protected override void OnEnable()
    {
        base.OnEnable();
        camTransform = GetComponentInChildren<Camera>().transform;
        camPosition = new Vector3(camTransform.position.x, camTransform.position.y, camTransform.position.z);
        camRotation = new Quaternion(camTransform.rotation.x, camTransform.rotation.y, camTransform.rotation.z, camTransform.rotation.w);
    }
    private void FixedUpdate()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            rigplayer.AddForce(agility * Time.deltaTime, 0, 0);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            rigplayer.AddForce(-agility * Time.deltaTime, 0, 0);
        }
        if (rigplayer.transform.position.y < gameManager.GetFallOut && playerOut == false)
        {
            playerOut = true;
            UnfixedCam();
            Invoke("ResetScene", timeToRespawn);
        }
    }

    private void UnfixedCam()
    {
        if (camTransform)
        {
            camTransform.parent = null;
        }
    }

    public  void ResetScene()
    {
        //gameManager.GamePause = true;
        //gameManager.ResetScene();
        //base.ResetPlayer();
        gameManager.ResetScene();
        camTransform.position = camPosition;
        camTransform.rotation = camRotation;
        camTransform.SetParent(transform);
        //List<Players> lplayers = gameManager.GetListPlay;
        //foreach (Players pl in lplayers)
        //{
        //    pl.ResetPlayer();
        //}
        //playerOut = false;
    }
}
