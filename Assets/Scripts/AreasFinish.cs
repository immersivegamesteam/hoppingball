﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreasFinish : Areas
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Players>() && gameManager.ShouldBePlayingGame())
        {
            if (gameManager.FinishGame)
                FinishGame();
        }
    }

    private void FinishGame()
    {
        // programação de finalização
    }
}
