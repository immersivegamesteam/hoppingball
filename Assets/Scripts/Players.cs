﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public abstract class Players : MonoBehaviour
{
    [SerializeField]
    private float acceleration = 1;
    protected Rigidbody rigplayer;
    private float _acceleration;
    private Vector3 startPosition;
    [SerializeField]
    public bool playerOut;

    protected GameManager gameManager;


    public float GetAcceleration { get { return acceleration; } }

    protected virtual void OnEnable()
    {
        gameManager = GameManager.Instance;
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        rigplayer = GetComponent<Rigidbody>();
        gameManager.AddPlayerList(this);
        _acceleration = acceleration;
    }

    public void SpeedyUp(float accel, float acceltime)
    {
        _acceleration = acceleration;
        acceleration *= accel;
        Invoke("NormalSpeedy", acceltime);
    }
    private void NormalSpeedy()
    {
        acceleration = _acceleration;
    }

    public virtual void ResetPlayer()
    {
        rigplayer.velocity = Vector3.zero;
        this.transform.position = startPosition;
        this.transform.rotation = Quaternion.identity;
        playerOut = false;
        this.gameObject.SetActive(true);
    }
}
